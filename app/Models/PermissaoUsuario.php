<?php

namespace App\Models;
 

class PermissaoUsuario extends RModel
{
    protected $table = "permissao_usuarios";
    protected $fillable   = ['permissao_id', 'usuario_id']; 
}
