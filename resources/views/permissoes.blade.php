@extends('layout')
@section('conteudo')
    <h1>Permissões do Sistema</h1>
    
       <table class="table">
        <thead>
                 
                <th>Nome</th>
                <th>Permissões</th>
                <th>Cadastro</th>
                <th>Compra</th>  
        </thead>
        <tbody> 
            @foreach($permissoes as $p)
                <tr>  
                    <td>{{ $p->nome_permissao }}</td>
                    <td><input value="{{ $p->permissoes }}" type="checkbox" @if($p->permissoes == 1) checked @endif  /></td>
                    <td><input value="{{ $p->cadastro }}" type="checkbox"   @if($p->cadastro == 1) checked @endif  /></td>
                    <td><input value="{{ $p->compras }}" type="checkbox"     @if($p->compras == 1) checked @endif  ></td>
                     
                </tr> 
             @endforeach
        </tbody> 
       </table> 
@endsection

