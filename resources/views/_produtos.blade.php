@extends('layout')
@section('conteudo')
    @if(isset($lista)   && count($lista) > 0)
        <div class="row">
            @foreach($lista as $prod)
                <div class="col-3 mb-3 d-flex align-items-stretch">
                    <div class="card">
                        <img src="{{ asset($prod->foto) }}" class="car-img-top">
                        <div class="card-body">
                            <h6 class="card-title">{{ $prod->nome }} - R$ {{ $prod->valor }}</h6>
                            <a href="{{ route('adicionar_carrinho', ['idproduto' => $prod->id]) }}" class="btn btn-sm btn-secondary">Adicionar</a>
                        </div>
                    </div>
                </div>
            @endforeach 
    </div>
    @endif
@endsection