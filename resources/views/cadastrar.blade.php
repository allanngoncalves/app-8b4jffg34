@extends('layout')
@section('scriptjs')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.11.2/jquery.mask.min.js" integrity="sha512-Y/GIYsd+LaQm6bGysIClyez2HGCIN1yrs94wUrHoRAD5RSURkqqVQEU6mM51O90hqS80ABFTGtiDpSXd2O05nw==" crossorigin="anonymous"></script>
<script>
$(document).ready(function(){
    $("#cpf").mask("000.000.000-00");
        $("#cep").mask("0000-000");
    $('.infoUser').on('click',  function(){
        let id = $(this).attr('data-value'); 
        $("#idusuario").val(id);
        $.post('{{ route("busca_usuario" ) }}', {idusuario: id }, function(user){
            user = JSON.parse(user); 
            $("#nome").val(user['nome']);
            $("#email").val(user['email']);
            $("#cpf").val(user['cpf']); 
            $("#logradouro").val(user['logradouro']);
            $("#numero").val(user['numero']);
            $("#complemento").val(user['complemento']);
            $("#cep").val(user['cep']);
            $("#cidade").val(user['cidade']);
            $("#estado").val(user['estado']); 
            $('#permissao option[value='+user['permissao_id']+']').prop('selected', true);
            $("#flag").val(0); 
        });
    });
    
    $('.delete').on('click',  function(){
        let id = $(this).attr('data-value');
        alert(id);
        $("#idUser").val(id);
    });
    
    $('#novo').on('click',  function(){
        $("#flag").val(1);
        $('#form').each (function(){
            this.reset();
        });
    });
});
</script>
@endsection 
@section('conteudo')
<div class="row">
<div class="col-2">
    <h2>Cadastrar</h2>
    </div>
    <div class="col-4">
        <button type="buttom" id="novo" class="btn btn-info" data-toggle="modal" data-target="#modal">Novo</button>
    </div>                 
</div> 
    <table class="table table-bordered">
        <thead>

            <th>Nome</th>
            <th>E-mail</th>
            <th>CPF</th>
            <th>Ações</th>

        </thead>
        <tbody>
            @foreach($lista as $user)
            <tr>
                <td>{{ $user->nome }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->cpf }}</td>
                <td>
                    <a href="#" class="btn btn-sm btn-info infoUser" data-value="{{ $user->id }}" data-toggle="modal" data-target="#modal">
                        <i class="fas fa-edit"></i>
                    </a>
                    <a href="#" class="btn btn-sm btn-danger delete" data-value="{{ $user->id }}" data-toggle="modal" data-target="#modalexcluir">
                        <i class="fas fa-trash"></i>
                    </a>
                </td>
            </tr>

            @endforeach

        </tbody>

    </table>
<div class="modal fade" id="modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Editar</h5>
            </div>
            <div class="modal-body">
                <div id="conteudo">
                   @include('form_usuario')

                </div>
            </div>
            <div class="modal-footer">
                <button type="buttom" class="btn btn-sm btn-secondary" data-dismiss="modal">Fechar</button>
            </div>

        </div>
    </div>
</div>
<div class="modal fade" id="modalexcluir">
    <div class="modal-dialog">
        <div class="modal-content">
        <form action="{{ route('excluir_usuario') }}" method="post">
        @csrf
             <input type="hidden" value="" name="id" id="idUser">
            <div class="modal-header">
                <h5 class="modal-title">Excluir</h5>
            </div>
            <div class="modal-body">
                <div id="conteudo"> 
                    <p>Você realmente quer excluir este usuário?</p> 
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-sm btn-danger ">Sim</a>
                <button type="buttom" class="btn btn-sm btn-secondary" data-dismiss="modal">Fechar</button>
            </div>
            </form>
        </div>
    </div>
</div>
@endsection