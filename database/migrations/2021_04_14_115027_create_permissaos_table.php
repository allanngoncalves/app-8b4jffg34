<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreatePermissaosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permissaos', function (Blueprint $table) {
            $table->increments("id");
            $table->string("nome_permissao", 100);
            $table->char("permissoes", 1);
            $table->char("cadastro", 1);
            $table->char("compras", 1);

            $table->timestamps();
        });

        DB::table('permissaos')->insert(  array(   'nome_permissao' => 'Administrador', 'permissoes' => '1','cadastro' => '1','compras' => '1' ));
        DB::table('permissaos')->insert(  array(   'nome_permissao' => 'Suporte', 'permissoes' => '0','cadastro' => '1','compras' => '0' ));
        DB::table('permissaos')->insert(  array(   'nome_permissao' => 'Cliente', 'permissoes' => '0','cadastro' => '0','compras' => '1' )); 
    }

    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permissaos');
    }
}
