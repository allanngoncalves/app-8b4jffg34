<?php

namespace App\Service;

use App\Models\Endereco;
use App\Models\PermissaoUsuario;
use App\Models\Usuario;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
class ClienteService
{

    public function salvarUsuario(Usuario $usuario, Endereco $endereco, PermissaoUsuario $permissaoUsuario ){
        try{
            $dbusuario = Usuario::where('email', $usuario->email)->first();
            if($dbusuario){
                return ['status' => 'err', 'message' => 'E-mail já cadastrado no sistema.'];
            }
            DB::beginTransaction();
                $res = $usuario->save(); 
                $endereco->usuario_id = $usuario->id; 
                $endereco->save();
                $permissaoUsuario->usuario_id = $usuario->id; 
                $permissaoUsuario->save();
            DB::commit();

            return ['status' => 'ok', 'message' => 'Usuário cadastrado com sucesso.'];
         }catch(\Exception $e){
             \Illuminate\Support\Facades\Log::error('ERRO', ['local' => 'ClienteService.salvarUsuario'], ['mensage' => $e->getMessage()]);
            DB::rollback();
            return ['status' => 'err', 'message' => 'Não pode cadastrar o usuário.'];
         }
    }
 
 


}