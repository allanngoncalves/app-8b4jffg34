<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateCategoriasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categorias', function (Blueprint $table) {
            $table->increments("id");
            $table->string("categoria", 100);

            $table->timestamps();
           
        });
        
        DB::table('categorias')->insert(  array(   'categoria' => 'Perfumes' ));
        DB::table('categorias')->insert(  array(   'categoria' => 'Esportes' ));
        DB::table('categorias')->insert(  array(   'categoria' => 'Relógios' ));
        DB::table('categorias')->insert(  array(   'categoria' => 'Eletros' ));
        DB::table('categorias')->insert(  array(   'categoria' => 'Eletrônicos' ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categorias');
    }
}
