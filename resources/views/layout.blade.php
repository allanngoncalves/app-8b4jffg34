<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}"> 
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>Loja Virtual</title> 
    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" rel="stylesheet">
    <link href=" https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    @yield('scriptjs')
</head>

<body>
    <nav class="navbar navbar-light navbar-expand-md bg-light pl-5 pr-5 mb-5">
        <a href="#" class="navbar-brand">Loja Virutal</a>
        <div class="collapse navbar-collapse">
            <div class="navbar-nav">
                <a class="nav-link" href="{{ route('home') }}">Home</a>
                <a class="nav-link" href="{{ route('categoria') }}">Produtos</a> 
            </div> 
        </div>
       
                @if(!Auth::user())
                    <a class="nav-link" href="{{ route('logar') }}"><i class="fas fa-user"></i></a>
                @else 
                     @if(Session::get('permissoes'))
                         <a class="nav-link" href="{{ route('permissoes') }}">Permissões</a>
                     @endif
                     @if(Session::get('cadastro'))
                        <a class="nav-link" href="{{ route('cadastrar') }}">Cadastrar</a>
                     @endif
                     @if(Session::get('compras'))
                        <a class="nav-link" href="{{ route('compras_historico') }}">Minhas Compras</a> 
                        <a href="{{ route('ver_carrinho') }}" class="btn btn-sm"><i class="fa fa-shopping-cart"></i></a>
                     @endif
                    
                    
                    <a href="{{ route('sair') }}" class="btn btn-sm"><i class="fas fa-sign-out-alt"></i></a>
                @endif
       

    </nav> 
    <div class="container">
        <div class="row"> 
        @if(Auth::user())
        <div class="col-12">
            <p class="text-right">Seja bem vindo, {{ Auth::user()->nome }}, <a href="{{ route('sair') }}">Sair</a></p>
        </div>
        @endif

        @if($message = Session::get('err'))
            <div class="col-12">
                <div class="alert alert-danger">{{ $message }}</div>
            </div>
        @endif
        @if($message = Session::get('ok'))
            <div class="col-12">
                <div class="alert alert-success">{{ $message }}</div>
            </div>
        @endif

        @if(isset($flag))
            <div class="col-12"> 
                @yield('conteudo')
            </div>
        @else
            <div class="col-3"> 
                @yield('categoria')
            </div>
            <div class="col-9"> 
                @yield('conteudo')
            </div>
        @endif
           
        </div>
    </div>
</body> 
</html>