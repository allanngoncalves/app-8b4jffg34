<?php

namespace App\Http\Controllers;

use App\Models\Endereco;
use App\Models\Permissao;
use App\Models\PermissaoUsuario;
use App\Models\Usuario;
use App\Service\ClienteService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsuarioController extends Controller
{
    public function logar(Request $request){
        $data = ['flag' => 1];

        if($request->isMethod('POST')){
            $email = $request->input("email");
            $password = $request->input("password");

            $credential = ['email' => $email, 'password' => $password];
            if(Auth::attempt($credential)){
                $usuario = Usuario::join('permissao_usuarios', 'usuarios.id', '=', 'permissao_usuarios.usuario_id')
                ->join('permissaos', 'permissaos.id', '=', 'permissao_usuarios.permissao_id')
                ->where('usuarios.email', $email)
                ->get();
                 
                $request->session()->put('permissoes', $usuario[0]->permissoes);
                $request->session()->put('cadastro', $usuario[0]->cadastro);
                $request->session()->put('compras', $usuario[0]->compras);
                return redirect()->route('home');
            }else{
                $request->session()->flash('err', 'Usuário inválido!');
                return redirect()->route('logar');
            }
        }

        return view('logar', $data);
    }
    
    public function sair(){
        Auth::logout();
        return redirect()->route('logar');
    }

    public function busca_usuario(Request $request){
        $idusuario = $request->input("idusuario");
        $usuario = Usuario::join('enderecos', 'usuarios.id', '=', 'enderecos.usuario_id')
                          ->join('permissao_usuarios', 'permissao_usuarios.usuario_id', '=', 'usuarios.id')
                                    ->where('usuarios.id', $idusuario)
                                    ->get();
        
        return  json_encode($usuario[0]);
    }

    public function novoUsuario(Request $request){
        $values = $request->all();
        $usuario = new Usuario();
        $usuario->fill($values); 
        $cpf = $request->input('cpf', '');
        $usuario->tratarCpf($cpf);
        $senha = $request->input('password', '');
        $usuario->password = Hash::make($senha);
        
        $endereco =new Endereco($values); 
        $permissaoUsuario =new PermissaoUsuario($values);

        $clienteService = new ClienteService();
        $res = $clienteService->salvarUsuario($usuario, $endereco,$permissaoUsuario);
        $message = $res['message'];
        $status  = $res['status']; 
        $request->session()->flash($status, $message); 
    }

    public function atualizarUsuario(Request $request){ 
        try{
           
            DB::beginTransaction();  
                 $id   = $request->input('idusuario'); 
                $values = $request->all();
                $user = Usuario::find($id);  
                $user->fill($values); 
                $cpf = $request->input('cpf', '');
                $user->tratarCpf($cpf);
                $senha = $request->input('password', '');
                $user->password = Hash::make($senha); 
                $user->save();

                $endereco = Endereco::where('usuario_id', $id)->get(); 
                $end = Endereco::find($endereco[0]->id); 
                $end->fill($values); 
                $end->save();

                $id_da_permissao = PermissaoUsuario::where('usuario_id', $id)->get(); 
                $permissao = PermissaoUsuario::find($id_da_permissao[0]->id); 
                $permissao->fill($values); 
                $permissao->save();

            DB::commit();
            $request->session()->flash('ok', 'Usuário atualizado com sucesso.');  
         }catch(\Exception $e){
             \Illuminate\Support\Facades\Log::error('ERRO', ['local' => 'ClienteService.atualizarUsuario'], ['mensage' => $e->getMessage()]);
            DB::rollback();
            $request->session()->flash('err', 'Não pode atualizar o usuário.');  
         }  
    }

    public function excluir_usuario(Request $request){ 
        Usuario::where('id', '=', $request->input('idusuario'))->delete();
        $data = [];
        $data['flag'] = 1;
        $data['lista'] = $this->listarUsuarios();
        return view('cadastrar', $data);
    }

    public function listarUsuarios(){
        $lista = Usuario::all();  
        return $lista;
    }

    public function permissoes(Request $request){  
        $data = [];
        $data['flag'] = 1;
        $data['permissoes'] = Permissao::all();
        return view('permissoes', $data);
    }
}
