<?php

namespace App\Models;
 

class Endereco extends RModel
{
    protected $table = "enderecos";
    protected $fillable   = ['id','logradouro', 'numero', 'estado', 'cidade', 'cep','complemento' ];

    public function tratarCep($cep){
        $value = preg_replace('/[^0-9]/','', $cep);
        $this->attributes['cep'] = $value;
    }
}

