@extends('layout')
@section('categoria') 
<div class="list-group">
        @if(isset($listaCategoria) && count($listaCategoria) > 0) 
            <a class="list-group-item list-group-item-action @if(0 == $idCategoria) active @endif" href="{{ route('categoria') }}">Todos </a>
                @foreach($listaCategoria as $cat)
                    <a class="list-group-item list-group-item-action @if($cat->id == $idCategoria) active @endif" href="{{ route('categoria_por_id', ['idcategoria' => $cat->id ]) }}">{{ $cat->categoria }} </a>
                @endforeach  
        @endif    
        </div>
@endsection
@section('conteudo')
    @include('_produtos', ['lista' => $lista])
@endsection

