@extends('layout')
@section('scriptjs')
<script>
$(document).ready(function(){
  $('.infocompra').on('click',  function(){
    let id = $(this).attr('data-value'); 
    $.post('{{ route("compra_detalhes" ) }}', {idpedido: id }, function(result){
        $('#conteudopedido').html(result)
    });
  });
});
</script>
@endsection
@section('conteudo')
<div class="col-12">
    <h2>Minhas Compras</h2>
</div>
<div class="col-12">
    <table class="table table-bordered">
        <thead>

            <th>Data da Compra</th>
            <th>Situação</th>
            <th></th>

        </thead>
        <tbody>
        @foreach($lista as $ped)
                <tr>
                    <td>{{ $ped->datapedido->format('d/m/Y H:i:s') }}</td> 
                    <td>{{ $ped->statusDesc() }}</td>
                    <td>
                        <a href="#" class="btn btn-sm btn-info infocompra" data-value="{{ $ped->id }}" data-toggle="modal" data-target="#modalcompra">
                            <i class="fas fa-shopping-basket"></i>
                        </a>
                    </td>
                </tr>
               
                @endforeach

        </tbody>

    </table>
    <div class="modal fade" id="modalcompra">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title">Detalhes da Compra</h5>
                </div> 
                <div class="modal-body">
                    <div id="conteudopedido"></div>
                </div> 
                <div class="modal-footer">
                    <button type="buttom" class="btn btn-sm btn-secondary" data-dismiss="modal">Fechar</button>
                </div> 
            
            </div> 
        </div>
    </div>
</div>
@endsection