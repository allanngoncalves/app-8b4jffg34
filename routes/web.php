<?php

use App\Http\Controllers\ClienteController;
use App\Http\Controllers\ProdutoController;
use App\Http\Controllers\UsuarioController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::match(['get', 'post'], '/', [ProdutoController::class, 'index'])->name('home');
Route::match(['get', 'post'], '/categoria', [ProdutoController::class, 'categoria'])->name('categoria');
Route::match(['get', 'post'], '/{idcategoria}/categoria', [ProdutoController::class, 'categoria'])->name('categoria_por_id');

Route::match(['get', 'post'], '/cadastrar', [ClienteController::class, 'cadastrar'])->name('cadastrar');
Route::match(['get', 'post'], '/cliente/cadastrar', [ClienteController::class, 'cadastrarCLiente'])->name('cadastrar_cliente');
Route::match(['get', 'post'], '/logar', [UsuarioController::class, 'logar'])->name('logar');
Route::post('/usuario/busca', [UsuarioController::class, 'busca_usuario'])->name('busca_usuario');
Route::match(['get', 'post'], '/usuario/excluir', [UsuarioController::class, 'excluir_usuario'])->name('excluir_usuario');
Route::match(['get', 'post'], '/permissoes', [UsuarioController::class, 'permissoes'])->name('permissoes');
Route::get('/sair', [UsuarioController::class, 'sair'])->name('sair');


Route::match(['get', 'post'], '/{idproduto}/carrinho/adicionar', [ProdutoController::class, 'adicionarCarrinho'])->name('adicionar_carrinho');

Route::match(['get', 'post'], '/carrinho', [ProdutoController::class, 'verCarrinho'])->name('ver_carrinho');

Route::match(['get', 'post'], '/{indice}/excluir_item', [ProdutoController::class, 'excluirItem'])->name('excluir_item');

Route::post('/carrinho/finalizar', [ProdutoController::class, 'finalizar'])->name('carrinho_finalizar');
Route::match(['get', 'post'], '/compras/historico', [ProdutoController::class, 'historico'])->name('compras_historico');

Route::post('/compras/detalhes', [ProdutoController::class, 'detalhes'])->name('compra_detalhes');

Route::match(['get', 'post'], '/compras/pagar', [ProdutoController::class, 'pagar'])->name('pagar');


