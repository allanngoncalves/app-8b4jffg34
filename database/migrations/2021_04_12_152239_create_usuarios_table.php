<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->increments("id");
            $table->string("email", 100)->unique();
            $table->string("password", 255);
            $table->string("nome", 50);
            $table->string("cpf", 50);
            $table->timestamps();
        });

        DB::table('usuarios')->insert(  array(   'email' => 'teste@teste.com', 
        'password' => '$2y$10$kOE1T4JEBIQVhJMS4yyHhOWA82ThsE5eq.X.UX.cSFO27cv587Bf6',
        'nome' => 'Usuário do Sistema','cpf' => '09163818733' ));
      
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarios');
    }
}
