@extends('layout')
@section('conteudo')
@if(isset($listaCategoria) && count($listaCategoria) > 0)
<div class="list-group">
      @foreach($listaCategoria as $cat)
               <a href="{{ route('categoria_por_id', ['idcategoria' => $cat->id ]) }}">{{ $cat->categoria }} </a>
            @endforeach        
         
</div>
    @endif
@endsection