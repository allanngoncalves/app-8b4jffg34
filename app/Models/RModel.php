<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RModel extends Model
{
    use HasFactory;

    protected $primaryKey = "id";
    public $timeStamps   = "true"; //create_at and update_at
    public $incremeting   = "true";
    protected $fillable   = [];

    public function beforeSave(){
        return false;
    }

    public function save(array $option = []){
        try{
            
            if($this->beforeSave()){
                return true;
            }

            return parent::save($option);
        }catch(\Exception $e){
            throw  new \Exception($e->getMessage());
        }
    }

}
