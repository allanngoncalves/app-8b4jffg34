<form action="{{ route('cadastrar_cliente') }}" id="form" method="post">
                        @csrf
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    Nome: <input type="text" name="nome" id="nome" class="form-control" />
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    E-mail: <input type="email" name="email" id="email" class="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-4">
                                <div class="form-group">
                                    CPF: <input type="text" name="cpf" id="cpf" class="form-control" />
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    Senha: <input type="text" name="password" id="password" class="form-control" />
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    Permissão: 
                                    <select class="form-control" name="permissao_id" id="permissao">
                                        <option>--SELECIONAR--</option>
                                        @foreach($permissoes as $p)
                                        <option value="{{ $p->id }}">{{$p->nome_permissao }}</option> 
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-7">
                                <div class="form-group">
                                    Endereço: <input type="text" name="logradouro" id="logradouro" class="form-control" />
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="form-group">
                                    Número: <input type="text" name="numero" id="numero" class="form-control" />
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-group">
                                    Complemento: <input type="text" name="complemento" id="complemento" class="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-4">
                                <div class="form-group">
                                    Cep: <input type="text" name="cep" id="cep" class="form-control" />
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    Cidade: <input type="text" name="cidade" id="cidade" class="form-control" />
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    Estado: <input type="text" name="estado" id="estado" class="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="hidden" value="1" name="flag" id="flag">
                            <input type="hidden" value="" name="idusuario" id="idusuario">
                            <input type="submit" value="Salvar" class="btn btn-success btn-sm">
                        </div>
                    </form>