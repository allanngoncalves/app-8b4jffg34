<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreatePermissaoUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permissao_usuarios', function (Blueprint $table) {
            $table->increments("id");
            $table->integer("permissao_id")->unsigned();
            $table->integer("usuario_id")->unsigned();

            $table->timestamps();

            $table->foreign("permissao_id")
                    ->references("id")->on("permissaos")
                    ->onDelete("cascade");

            $table->foreign("usuario_id")
                    ->references("id")->on("usuarios")
                    ->onDelete("cascade");        
        });

        DB::table('permissao_usuarios')->insert(array('permissao_id' => 1, 'usuario_id' => 1)); 
   
    }

    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permissao_usuarios');
    }
}
