<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateEnderecosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enderecos', function (Blueprint $table) {
            $table->increments("id");

            $table->string("logradouro");
            $table->string("numero");
            $table->string("estado");
            $table->string("cidade");
            $table->string("cep");
            $table->string("complemento")->nullable(); 
            $table->integer("usuario_id")->unsigned();
            $table->timestamps();
            
            $table->foreign("usuario_id")
                        ->references("id")->on("usuarios")
                        ->onDelete("cascade");
        });

        DB::table('enderecos')->insert(  array(   
            'logradouro' => 'Rua José da Costa', 
            'numero' => '22',
            'estado' => 'São Paulo',
            'cidade' => 'Niterói',
            'cep' => '24240181',
            'complemento' => 'casa',
            'usuario_id' => 1
        ));
      
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enderecos');
    }
}
