<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateProdutosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produtos', function (Blueprint $table) {
            $table->increments("id");
            $table->string("nome", 100);
            $table->decimal("valor", 10,2);
            $table->string("foto", 100)->nullable();
            $table->string("descricao", 255)->nullable();
            $table->integer("categoria_id")->unsigned();
            $table->timestamps();
            
            $table->foreign("categoria_id")
                                    ->references("id")->on("categorias")
                                    ->onDelete("cascade");
        });

        DB::table('produtos')->insert(
            array('nome' => 'Iphone 11','valor' => 10.00,'foto' => 'images/iphone11.jpg','descricao' => '','categoria_id' => 5 ),       
        );
        DB::table('produtos')->insert(
            array('nome' => 'Geladeira Electrolux','valor' => 10.00,'foto' => 'images/geladeiraEletrolux1.jpg','descricao' => '','categoria_id' => 4 ),      
        );
        DB::table('produtos')->insert(
            array('nome' => 'Tênis Adidas','valor' => 10.00,'foto' => 'images/tenis-adidas-1.jpg','descricao' => '','categoria_id' => 2 ),     
        );
        DB::table('produtos')->insert(
            array('nome' => 'Perfume 1 Million','valor' => 10.00,'foto' => 'images/perfume-1million.jpg','descricao' => '','categoria_id' => 1 ),            
        );
        DB::table('produtos')->insert(
              array('nome' => 'Relógio Invicta','valor' => 10.00,'foto' => 'images/relogio-invicta-1.jpg','descricao' => '','categoria_id' => 3 ),
        );
        DB::table('produtos')->insert(
             array('nome' => 'Micro-Ondas Electrolux','valor' => 10.00,'foto' => 'images/microondas-eletrolux-1.jpg','descricao' => '','categoria_id' => 4 ), 
        );
        

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produtos');
    }
}
