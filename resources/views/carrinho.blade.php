@extends('layout')
@section('conteudo')
    <h1>Carrinho</h1>
    @if(isset($cart)   && count($cart) > 0)
       <table class="table">
        <thead>
                <th></th>
                <th>Nome</th>
                <th>Foto</th>
                <th>Valor</th>
                <th>Descrição</th>
        </thead>
        <tbody>
            @php $total = 0; @endphp
            @foreach($cart as $indice => $p)
                <tr>
                    <td>
                        <a href="{{ route('excluir_item', ['indice' => $indice]) }}" class="btn btn-danger btn-sm">
                            <i class="fa fa-trash"></i>
                        </a>
                    </td>
                    <td>{{ $p->nome }}</td>
                    <td><img src="{{ $p->foto }}" height="35"></td>
                    <td>{{ $p->valor }}</td>
                    <td>{{ $p->descricao }}</td>
                </tr>
                @php $total += $p->valor; @endphp
                @endforeach
        </tbody>
        <tfooter>
            <tr>
                <td colspan="5">
                    Total: R$ {{ $total }}
                </td>
            </tr>
        </tfooter>
       </table>
    <div class="pull-right">
       <form action="{{ route('carrinho_finalizar') }}" method="post">
       @csrf
         <input type="submit" value="Finalizar Compra" class="btn btn-lg btn-success">
       </form>
    </div>
    @else
        <p>Nenhum item no carrinho</p>    
    @endif
@endsection

