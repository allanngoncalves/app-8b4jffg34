<?php

namespace App\Service;
 
use App\Models\ItensPedido;
use App\Models\Pedido;
use App\Models\Usuario;
use DateTime;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
class VendaService
{

    public function finalizarVenda($produtos = [], Usuario $usuario){
        try{
             
            DB::beginTransaction();
                $dataHj = new DateTime();
                $pedido =  new Pedido();
                $pedido->datapedido = $dataHj->format('Y-m-d H:i:s');
                $pedido->status = 'PEN';
                $pedido->usuario_id = $usuario->id;
                $pedido->save();
                foreach($produtos as $p){
                    $itens = new ItensPedido();
                    $itens->quantidade = 1;
                    $itens->valor = $p->valor;
                    $itens->dt_item = $dataHj->format('Y-m-d H:i:s');;
                    $itens->produto_id = $p->id;
                    $itens->pedido_id = $pedido->id;
                    $itens->save();
                } 
            DB::commit();  
            return ['status' => 'ok', 'message' => 'Venda finalizada com sucesso.'];
         }catch(\Exception $e){
            Log::error('ERRO', ['local' => 'ClienteService.salvarUsuario'], ['mensage' => $e->getMessage()]);
            DB::rollback();
            return ['status' => 'err', 'message' => 'VEnda não pode ser finalizada']; 
         }
    }


}