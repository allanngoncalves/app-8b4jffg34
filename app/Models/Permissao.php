<?php

namespace App\Models;

class Permissao extends RModel
{
    protected $table = "permissaos";
    protected $fillable   = ['nome_permissao', 'permissoes', 'cadastro', 'compras']; 
}
