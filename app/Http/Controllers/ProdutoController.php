<?php

namespace App\Http\Controllers;

use App\Models\Categoria;
use App\Models\ItensPedido;
use App\Models\Pedido;
use App\Models\Produto;
use App\Service\VendaService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProdutoController extends Controller
{
    public function index(Request $request){
        $data = [];
        $listaProdutos = Produto::all(); 
        $data['lista'] = $listaProdutos;
        $data['flag'] = 1;
        return view('home', $data);
    }

    public function categoria($idcategoria = 0,Request $request){
        $data = [];
        $listaCategorias = Categoria::all(); 
        $queryProdutos = Produto::limit(10000);

        if($idcategoria != 0){
            $queryProdutos->where('categoria_id', $idcategoria);
        }
        $listaProdutos = $queryProdutos->get();
        $data['lista'] = $listaProdutos;
        $data['listaCategoria'] = $listaCategorias;
        $data['idCategoria'] = $idcategoria;
        return view('categoria', $data);
    }

    public function adicionarCarrinho($idProduto = 0, Request $request){
        $prod = Produto::find($idProduto);

        if($prod){
            
            //buscar da sessão o carrinho atual
            $carrinho =  session('cart', []);
            array_push($carrinho, $prod); 
            session(['cart'=> $carrinho]);
        }
       
        return redirect()->route("home");
    }

    public function verCarrinho(Request $request){
        $carrinho =  session('cart', []);
        $data = ['cart' => $carrinho];
        $data['flag'] = 1;
        return view('carrinho', $data);
    }

    public function excluirItem($indice, Request $request)
    {
        $carrinho =  session('cart', []);
        if(isset($carrinho[$indice])){
            unset($carrinho[$indice]);
        }
        session(['cart'=> $carrinho]);
        return redirect()->route("ver_carrinho");
    }

    public function finalizar(Request $request)
    {
        $produtos =  session('cart', []);
        $vendaService = new VendaService();
        $res = $vendaService->finalizarVenda($produtos, Auth::user());

        if($res['status'] == 'ok'){
            $request->session()->forget('cart');
        }        

        $request->session()->flash($res['status'],$res['message']);

        return redirect()->route("ver_carrinho");
    }

    public function historico(){
        $data = [];
        $data['flag'] = 1;

        $idUsuario = Auth::user()->id;

        $listaPedido = Pedido::where('usuario_id', $idUsuario)
                                ->orderBy('datapedido', 'desc')
                                ->get();
        $data['lista'] = $listaPedido;

        return view('compra/historico', $data);
    }

    public function detalhes(Request $request){ 
        $idpedido = $request->input("idpedido");
        $listaItens = ItensPedido::join('produtos', 'produtos.id', '=', 'itens_pedidos.produto_id')
                                    ->where('pedido_id', $idpedido)
                                    ->get(['itens_pedidos.*', 'itens_pedidos.valor as valoritem', 'produtos.*']);
        $data = [];
        $data['listaitens'] = $listaItens;
        return view('compra/detalhes', $data);
     }
     public function pagar(Request $request){ 
        $data = [];
        $data['flag'] = 1;

        return view('compra/pagar', $data);
     }
}
