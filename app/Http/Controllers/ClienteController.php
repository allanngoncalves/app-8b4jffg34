<?php

namespace App\Http\Controllers;

use App\Models\Endereco;
use App\Models\Permissao;
use App\Models\Usuario;
use Illuminate\Http\Request; 
use Illuminate\Support\Facades\Hash;
use App\Service\ClienteService;
use PhpParser\Node\Stmt\Return_;

class ClienteController extends UsuarioController
{
    public function cadastrar(Request $request){
        $data = [];
        $data['flag'] = 1;
        $data['lista'] = $this->listarUsuarios();
        $data['permissoes'] = Permissao::all();
        return view('cadastrar', $data);
    }

    function cadastrarCliente(Request $request)
    {
        $flag = $request->input('flag', '');
        if($flag){
            UsuarioController::novoUsuario($request); 
        }else{ 
            UsuarioController::atualizarUsuario($request); 
        }
        $data['permissoes'] = Permissao::all();
        $data['lista'] = $this->listarUsuarios(); 
        $data['flag'] = 1; 
        return view('cadastrar', $data);
    }
  
}
